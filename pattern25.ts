var star: string = ''
var baris: number = 5
var frontSpace: number = 0

for (var i:number = 0; i < baris; i++) {
   star = ''
   for (let i = 0; i < frontSpace; i++) {
      star = star + ' '  
   }
   if (i == 0 || i == baris-1) {
      for (var j:number = 0; j < baris; j++) {
         star = star + '*'         
      }
   } else {
      for (var j:number = 0; j < baris; j++) {
         if (j == 0 || j == baris-1) {
            star = star + '*'
         } else {
            star = star + ' '
         }         
      }
   }
   frontSpace++
   console.log(star)
}
